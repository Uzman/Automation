<!DOCTYPE html>
<html>


<head>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<?php
/**
 * Created by PhpStorm.
 * User: oguz
 * Date: 05/04/15
 * Time: 16:42
 */
include "utils.php";

$teacher_id = getParameter("teacher_id");
if($teacher_id == -1){
    die("course id must be provided");
}
$teacherSchedule = runQuery("
    SELECT s.dayOfWeek, s.hourOfDay, c.description,c.title
    FROM Teacher teacher, Teach teach, schedule s, Course c
    WHERE teacher.tid = $teacher_id AND teach.tid = teacher.tid AND teach.cid = c.cid AND c.cid = s.cid;");
?>


<table class="scheduler">
    <tr>
        <th></th>
        <th>Monday</th>
        <th>Tuesday</th>
        <th>Wednesday</th>
        <th>Thursday</th>
        <th>Friday</th>
    </tr>
    <?php
    $rows = "";
    $assocArray = array();
    while($class = $teacherSchedule->fetch_assoc()){
        $currentHourOfDay = (int)$class["hourOfDay"];
        $currentDay = (int)$class["dayOfWeek"];

        $index =  24*$currentDay + $currentHourOfDay;

        $assocArray[$index] = $class;

    }

    $rows .= createRowsTeacher($assocArray);



    echo $rows;

    ?>


</table>
<?php

function createRowsTeacher($array){

    $rows = "";

    for($hour = 8; $hour < 17; $hour++){
        $rows .= "<tr><td>$hour</td>";
        for($day = 1; $day < 6; $day++){
            $rows .= "<td>";
            if(isset($array[24*$day+$hour])){
                $element = $array[24*$day+$hour];

                $rows .= "<a>".$element["description"]."<br>".$element["title"]."</a>";
            }
            $rows .= "</td>";
        }
        $rows .= "</tr>";
    }
    return $rows;

}
printBanner();
?>

</body>
</html>