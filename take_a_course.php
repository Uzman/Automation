<!DOCTYPE html>
<html>


<head>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<iframe name="take_course_result" src="" width="%100" height="50" frameBorder="0" >
</iframe>
<br>
<?php
/**
 * Created by PhpStorm.
 * User: oguz
 * Date: 02/04/15
 * Time: 19:58
 */

include 'utils.php';
$departmentID;


$departmentID = getParameter("department_id");
$studentID = getParameter("student_id");

if($studentID == -1){
    die("Student ID must be provided.");
}

echo $studentID."<br>".$departmentID;

$studentDetail = runQuery("
SELECT * FROM Student s WHERE s.sid = $studentID;");

if($detail = $studentDetail->fetch_assoc()){
    $detailText = "Student No:";
    $detailText .= $detail["sid"]." ";
    $detailText .= "Full Name:";
    $detailText .= $detail["fname"]." ".$detail["lname"];
    ?>
    <div>
        <a></a>
    </div>
    <?php
}

echo $detailText;

?>
<form action="take_a_course.php" method="post">
    <input name="student_id" hidden="true" value="<?php echo $studentID ?>">
    <select  name="department_id">
        <?php
        $dropdown = "";

        $departmentList = runQuery("SELECT D.dname, D.did FROM Department D;");

        while ($linea = $departmentList->fetch_assoc()) {

            $selected = "";
            $dropdown .="<option ".($linea["did"] == $departmentID ? "selected ='selected'" : '')." value=".$linea["did"].">".$linea["dname"]."</option>";

        }
        echo $dropdown;

        ?>
    </select>
    <input type="submit" value="Show courses">
</form>
<?php

if($departmentID!= -1 && $studentID != -1) {
    $coursesTable;

    $courses = runQuery("SELECT s.*,c.*,r.description as roomName,teacher.fname as teacherName, teacher.lname as teacherLastName
FROM Course c, schedule s, Room r, Teach teach, Teacher teacher
WHERE $departmentID = c.did AND c.did = s.cid AND r.rid = s.rid AND teach.cid = c.cid
      AND teach.tid = teacher.tid GROUP BY c.cid, r.rid;");
    $tableHTML = "<table>";

    $tableHTML .= "<tr>";
    $tableHTML .= "<th>Course Code /Class</th>";
    $tableHTML .= "<th>Course Name</th>";
    $tableHTML .= "<th>Credit</th>";
    $tableHTML .= "<th>Lecturer</th>";
    $tableHTML .= "<th>Add</th>";
    $tableHTML .= "</tr>";


    while($courseDetail = $courses->fetch_assoc()){
        $tableHTML .= "<tr>";

        $tableHTML .= "<td>";
        $tableHTML .= $courseDetail["roomName"]."/".$courseDetail["description"];
        $tableHTML .= "</td>";

        $tableHTML .= "<td>";
        $tableHTML .= $courseDetail["title"];
        $tableHTML .= "</td>";

        $tableHTML .= "<td>";
        $tableHTML .= $courseDetail["credits"];
        $tableHTML .= "</td>";

        $tableHTML .= "<td>";
        $tableHTML .= $courseDetail["teacherName"]." ".$courseDetail["teacherLastName"];
        $tableHTML .= "</td>";

        $courseID = $courseDetail["cid"];

        $addLink = "services/save_course_to_student.php?student_id=$studentID&course_id=$courseID";

        $tableHTML .= "<td>";

        $tableHTML .= "<a href='$addLink' target='take_course_result'>Add</a>";

        $tableHTML .= "</td>";

        $tableHTML .= "</tr>";

    }

    $tableHTML .= "</table>";

    echo $tableHTML;
}

printBanner();
?>

<script>


</script>

</body>
</html>