<!DOCTYPE html>
<html>


<head>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<div class="wrapper">
    <div id="one" class="div_left">
        <?php
        include 'utils.php';
        printBanner();?>
    </div>
    <div id="two" class="div_right" style='text-align:center'>
        <?php
        /**
         * Created by PhpStorm.
         * User: oguz
         * Date: 05/04/15
         * Time: 16:34
         */
        $departmentID = getParameter("department_id");

        ?>


        <form action="teacher_list.php" method="post">
            <select  name="department_id">
                <?php
                $dropdown = "";

                $departmentList = runQuery("SELECT D.dname, D.did FROM Department D;");

                while ($linea = $departmentList->fetch_assoc()) {

                    $selected = "";
                    $dropdown .="<option ".($linea["did"] == $departmentID ? "selected ='selected'" : '')." value=".$linea["did"].">".$linea["dname"]."</option>";
                }
                echo $dropdown;

                ?>
            </select>
            <input type="submit" value="Show teachers">
        </form>

        <?php
        if($departmentID!= -1 ) {
            $teachersTable;
            $teachers = runQuery("    SELECT * FROM Teacher WHERE Teacher.did = $departmentID;");
            $tableHTML = "<table style='display: inline-block'>";

            $tableHTML .= "<tr>";
            $tableHTML .= "<th> Name</th>";
            $tableHTML .= "<th>Last Name</th>";
            $tableHTML .= "<th>Show Schedule</th>";
            $tableHTML .= "</tr>";


            while ($teacherDetail = $teachers->fetch_assoc()) {
                $tableHTML .= "<tr>";

                $tableHTML .= "<td>";
                $tableHTML .= $teacherDetail["fname"] ;
                $tableHTML .= "</td>";

                $tableHTML .= "<td>";
                $tableHTML .= $teacherDetail["lname"];
                $tableHTML .= "</td>";

                $teacherID = $teacherDetail["tid"];

                $tableHTML .= "<td>";
                $tableHTML .= "<a href='teacher_schedule.php?teacher_id=$teacherID'>Schedule</a>";
                $tableHTML .= "</td>";


                $tableHTML .= "</tr>";

            }

            $tableHTML .= "</table>";

            echo $tableHTML;
        }

        ?>
    </div>
</div>