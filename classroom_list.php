<head>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<?php
/**
 * Created by PhpStorm.
 * User: oguz
 * Date: 05/04/15
 * Time: 02:17
 */
include 'utils.php';


$departmentID = getParameter("department_id");
?>


    <form action="classroom_list.php" method="post">
        <select  name="department_id">
            <?php
            $dropdown = "";

            $departmentList = runQuery("SELECT D.dname, D.did FROM Department D;");

            while ($linea = $departmentList->fetch_assoc()) {

                $selected = "";
                $dropdown .="<option ".($linea["did"] == $departmentID ? "selected ='selected'" : '')." value=".$linea["did"].">".$linea["dname"]."</option>";
            }
            echo $dropdown;

            ?>
        </select>
        <input type="submit" value="Show classrooms">
    </form>

<?php
if($departmentID!= -1) {
    $coursesTable;
    $courses = runQuery("SELECT * FROM Room r WHERE r.did = $departmentID;");
    $tableHTML = "<table>";

    $tableHTML .= "<tr>";
    $tableHTML .= "<th>Class Name</th>";
    $tableHTML .= "<th>Weekly Schedule</th>";
    $tableHTML .= "</tr>";


    while ($courseDetail = $courses->fetch_assoc()) {
        $tableHTML .= "<tr>";

        $tableHTML .= "<td>";
        $tableHTML .= $courseDetail["description"] ;
        $tableHTML .= "</td>";

        $roomID = $courseDetail["rid"];

        $tableHTML .= "<td>";
        $tableHTML .= "<a href='classroom_schedule.php?classroom_id=$roomID'>Schedule</a>";
        $tableHTML .= "</td>";


        $tableHTML .= "</tr>";

    }

    $tableHTML .= "</table>";

    echo $tableHTML;
}

printBanner();
?>