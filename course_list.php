<head>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<?php
/**
 * Created by PhpStorm.
 * User: oguz
 * Date: 05/04/15
 * Time: 02:17
 */
include 'utils.php';


$departmentID = getParameter("department_id");
?>


    <form action="course_list.php" method="post">
        <select  name="department_id">
            <?php
            $dropdown = "";

            $departmentList = runQuery("SELECT D.dname, D.did FROM Department D;");

            while ($linea = $departmentList->fetch_assoc()) {

                $selected = "";
                $dropdown .="<option ".($linea["did"] == $departmentID ? "selected ='selected'" : '')." value=".$linea["did"].">".$linea["dname"]."</option>";
            }
            echo $dropdown;

            ?>
        </select>
        <input type="submit" value="Show courses">
    </form>

<?php
if($departmentID!= -1) {
    $coursesTable;
    $courses = runQuery("SELECT s.*,c.*,r.description as roomName, teacher.fname teacherName, teacher.lname as teacherLastName
FROM Course c, schedule s, Room r, Teacher teacher, Teach teach
WHERE $departmentID = c.did AND c.did = s.cid AND r.rid = s.rid AND c.cid = teach.cid AND teach.tid = teacher.tid
GROUP BY c.cid;
");
    $tableHTML = "<table>";

    $tableHTML .= "<tr>";
    $tableHTML .= "<th>Course Code /Class</th>";
    $tableHTML .= "<th>Course Name</th>";
    $tableHTML .= "<th>Credit</th>";
    $tableHTML .= "<th>Lecturer</th>";
    $tableHTML .= "<th>Show Schedule</th>";
    $tableHTML .= "</tr>";


    while ($courseDetail = $courses->fetch_assoc()) {
        $tableHTML .= "<tr>";

        $tableHTML .= "<td>";
        $tableHTML .= $courseDetail["roomName"] . "/" . $courseDetail["description"];
        $tableHTML .= "</td>";

        $tableHTML .= "<td>";
        $tableHTML .= $courseDetail["title"];
        $tableHTML .= "</td>";

        $tableHTML .= "<td>";
        $tableHTML .= $courseDetail["credits"];
        $tableHTML .= "</td>";

        $tableHTML .= "<td>";
        $tableHTML .= $courseDetail["teacherName"] . " " . $courseDetail["teacherLastName"];
        $tableHTML .= "</td>";

        $courseID = $courseDetail["cid"];

        $tableHTML .= "<td>";
        $tableHTML .= "<a href='course_schedule.php?course_id=$courseID'>Schedule</a>";
        $tableHTML .= "</td>";

        $tableHTML .= "</tr>";

    }

    $tableHTML .= "</table>";

    echo $tableHTML;
}

printBanner();
?>