<!DOCTYPE html>
<html>


<head>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<?php
/**
 * Created by PhpStorm.
 * User: oguz
 * Date: 05/04/15
 * Time: 02:17
 */

include "utils.php";

$classroomID = getParameter("classroom_id");
if($classroomID == -1){
    die("course id must be provided");
}
$classroomSchedule = runQuery("
    SELECT s.hourOfDay, s.dayOfWeek, c.description, c.title
    FROM Course c, schedule s, Room r
    WHERE c.cid = s.cid AND c.cid = $classroomID AND
    s.rid = r.rid;");
?>


    <table class="scheduler">
        <tr>
            <th></th>
            <th>Monday</th>
            <th>Tuesday</th>
            <th>Wednesday</th>
            <th>Thursday</th>
            <th>Friday</th>
        </tr>
        <?php
        $rows = "";
        $assocArray = array();
        while($class = $classroomSchedule->fetch_assoc()){
            $currentHourOfDay = (int)$class["hourOfDay"];
            $currentDay = (int)$class["dayOfWeek"];

            $index =  24*$currentDay + $currentHourOfDay;

            $assocArray[$index] = $class;

        }

        $rows .= createRowsForClassroomSchedule($assocArray);



        echo $rows;

        ?>


    </table>
<?php

function createRowsForClassroomSchedule($array){

    $rows = "";

    for($hour = 8; $hour <= 17; $hour++){
        $rows .= "<tr><td>$hour</td>";
        for($day = 1; $day < 6; $day++){
            $rows .= "<td>";
            if(isset($array[24*$day+$hour])){
                $element = $array[24*$day+$hour];

                $rows .= "<a>".$element["title"]."<br>".$element["description"]."<br>"."</a>";
            }
            $rows .= "</td>";
        }
        $rows .= "</tr>";
    }
    return $rows;

}
printBanner();
?>

</body>
</html>