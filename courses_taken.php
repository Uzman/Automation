<!DOCTYPE html>
<html>


<head>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<script>

    function removeRowFromParentWithID(id){
        var str = "course_"+id.toString();
        var row = document.getElementById(str);
        row.parentNode.removeChild(row);
    }

</script>
<iframe name="delete_course" src="" width="%100" height="50" frameBorder="0" >
</iframe>
<br>
<?php
/**
 * Created by PhpStorm.
 * User: oguz
 * Date: 04/04/15
 * Time: 20:20
 */


include 'utils.php';

$studentID = getParameter("student_id");
echo $studentID;

if($studentID == -1){
    die("Student ID must be provided");
}

$coursesList = runQuery("
    SELECT *
    FROM Take T, Course c WHERE T.sid = $studentID AND T.cid = c.cid ;");
?>
<table id="courses_list">
    <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Credit</th>
        <th>Delete</th>
    </tr>
    <?php
    $tableRows = "";
    $rowNum = 0;
    while($course = $coursesList->fetch_assoc()){


        $courseID = $course["cid"];

        $rowName = "course_".$rowNum;
        $row = "<tr id='$rowName'>";

        $row .= "<td>".$course["title"]."</td>";
        $row .= "<td>".$course["description"]."</td>";
        $row .= "<td>".$course["credits"]."</td>";
        $link = "services/delete_course_from_student.php?course_id=$courseID&student_id=$studentID";
        $row .= "<td>".'<a href='.$link.' target="delete_course"';
        $row .= ' onclick=\'removeRowFromParentWithID(';
        $row .= $rowNum.')\'>Remove</a></td>';

        $row .= "</tr>";
        $tableRows.=$row;
        $rowNum++;

    }

    echo $tableRows;
    ?>


    </table>
<?php

printBanner();
?>