<head>
    <link rel="stylesheet" type="text/css" href="main.css ">
</head>
<?php

/**
 * Created by PhpStorm.
 * User: oguz
 * Date: 02/04/15
 * Time: 00:06
 */

include 'utils.php';

$students = runQuery("SELECT * FROM Student;");

$out = '<table ">';
while ($field = $students->fetch_field()) $out .= "<th>" . $field->name . "</th>";
$out .= "<th class='th-flat'>Student Details</th>";
$out .= "<th class='th-flat'>Take course</th>";
$out .= "<th class='th-flat'>Show taken courses</th>";
$out .= "<th class='th-flat'>Course Schedule</th>";
while ($linea = $students->fetch_assoc()) {
    $out .= "<tr>";

    $out .= "<td class='td-flat'>";
    $out .= $linea["sid"];
    $out .= "</td>";

    $out .= "<td class='td-flat'>";
    $out .= $linea["fname"];
    $out .= "</td>";

    $out .= "<td class='td-flat'>";
    $out .= $linea["lname"];
    $out .= "</td>";

    $out .= "<td class='td-flat'>";
    $out .= $linea["birthdate"];
    $out .= "</td>";

    $out .= "<td class='td-flat'>";
    $out .= $linea["birthplace"];
    $out .= "</td>";

    $out .= "<td class='td-flat'>";
    $out .= $linea["did"];
    $out .= "</td>";

    $out .= "<td class='td-flat'>";
    $out .= $linea["TCKIMLIKNO"];
    $out .= "</td>";


    $link = "student_detail.php?student_id=".$linea["sid"];
    $out .= "<td class='td-flat'><a href ='$link'>Student Details</a></td>";

    $link = "take_a_course.php?student_id=".$linea["sid"];
    $out .= "<td class='td-flat'><a href ='$link'>Take Course</a></td>";

    $link = "courses_taken.php?student_id=".$linea["sid"];
    $out .= "<td class='td-flat'><a href ='$link'>Show taken courses</a></td>";

    $link = "student_schedule.php?student_id=".$linea["sid"];
    $out .= "<td class='td-flat'><a href ='$link'>Show course schedule</a></td>";

    $out .= "</tr>";
}
$out .= "</table>";
echo $out;
echo "wedq";
printBanner();
?>