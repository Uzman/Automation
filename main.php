<html>
<head>
<head>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
</head>
<body>
<table>

    <tr>
        <th><a href="main.php?action=list_students">Student list</a></th>
        <th><a href="main.php?action=add_course_to_student">Add course to student</a></th>
        <th><a href="main.php?action=list_teachers">Teacher list</a></th>
        <th><a href="main.php?list_classrooms">Classroom list</a></th>
    </tr>
</table>
<?php

$action = getParameter("action");

switch ($action) {

    case "list_students":
        listStudents();
        break;
    case "student_detail":
        studentDetail();
        break;
    case "available_courses":
        availableCourses();
        break;
    case "take_course":
        if (takeCourse() != 1) {
            die("An error occurred");
        } else {
            availableCourses();
        }
        break;
    case "courses_taken":
        coursesTaken();
        break;
    case "remove_course_from_student":
        if (removeCourseFromStudent() != 1) {
            die("An error occurred");
        } else {
            coursesTaken();
        }
        break;
    case "schedule_of_student":
        scheduleForStudent();
        break;
    case "schedule_of_teacher";
        scheduleForLecturer();
        break;
    case "schedule_of_classroom";
        classroomSchedule();
        break;
    case "schedule_of_course";
        courseSchedule();
        break;
    case "save_student_detail";
        if(saveStudent()==1){
            studentDetail();
        }
        break;
    default:
        listStudents();
        break;

}
?>
</body>



<?php

/**
 * Created by PhpStorm.
 * User: oguz
 * Date: 04/04/15
 * Time: 20:21
 */


function getParameter($parameterName)
{
    if (isset($_GET[$parameterName])) {
        return $_GET[$parameterName];
    } else {
        if (isset($_POST[$parameterName])) {
            return $_POST[$parameterName];
        } else {
            return -1;
        }
    }
}

function getConnectionToDatabase()
{
    // Create connection

//localpc
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "Automation";


    /*//freesqlhosting
        $servername = "sql3.freemysqlhosting.net";
        $username = "sql373121";
        $password = "rP8*gQ6%";
        $dbname = "sql373121";
    */


    //Ebuyusuf
    /*
        $servername = "localhost";
        $username = "bilgiden_oguz";
        $password = "~K=%V]~a#+r#";
        $dbname = "bilgiden_auto";
    */


    /*
    //000webhost, şu an çalışıyor
    $servername = "mysql6.000webhost.com";
    $username = "a5307168_oguz";
    $password = "oguz9305";
    $dbname = "a5307168_oguz";
    */

    /*
    //db4free local üzerindendenenince çalışıyor.
    $servername = "db4free.net";
    $username = "oguz_automation";
    $password = "oguz_automation";
    $dbname = "oguz_automation";
    */

    /*
        $servername = "localhost";
        $username = "oguz_automation";
        $password = "oguz_automation";
        $dbname = "oguz_automation";
    */
    ini_set('display_errors', 1);

    $conn = new mysqli($servername, $username, $password, $dbname, "3306");
// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } else {
        mysqli_set_charset($conn, 'utf8');
        return $conn;
    }

}

function runQuery($query)
{
    //echo $query;
    $connection = getConnectionToDatabase();
    return $connection->query($query);
}

function listStudents()
{

    $students = runQuery("SELECT * FROM Student;");

    $out = '<table ">';
    while ($field = $students->fetch_field()) $out .= "<th>" . $field->name . "</th>";
    $out .= "<th class='th-flat'>Student Details</th>";
    $out .= "<th class='th-flat'>Take course</th>";
    $out .= "<th class='th-flat'>Show taken courses</th>";
    $out .= "<th class='th-flat'>Course Schedule</th>";
    while ($linea = $students->fetch_assoc()) {
        $out .= "<tr>";

        $out .= "<td class='td-flat'>";
        $out .= $linea["sid"];
        $out .= "</td>";

        $out .= "<td class='td-flat'>";
        $out .= $linea["fname"];
        $out .= "</td>";

        $out .= "<td class='td-flat'>";
        $out .= $linea["lname"];
        $out .= "</td>";

        $out .= "<td class='td-flat'>";
        $out .= $linea["birthdate"];
        $out .= "</td>";

        $out .= "<td class='td-flat'>";
        $out .= $linea["birthplace"];
        $out .= "</td>";

        $out .= "<td class='td-flat'>";
        $out .= $linea["did"];
        $out .= "</td>";

        $out .= "<td class='td-flat'>";
        $out .= $linea["TCKIMLIKNO"];
        $out .= "</td>";


        $link = "main.php?action=student_detail&student_id=" . $linea["sid"];
        $out .= "<td class='td-flat'><a href ='$link'>Student Details</a></td>";

        $link = "main.php?action=available_courses&student_id=" . $linea["sid"];
        $out .= "<td class='td-flat'><a href ='$link'>Take Course</a></td>";

        $link = "main.php?action=courses_taken&student_id=" . $linea["sid"];
        $out .= "<td class='td-flat'><a href ='$link'>Show taken courses</a></td>";

        $link = "main.php?action=schedule_of_student&student_id=" . $linea["sid"];
        $out .= "<td class='td-flat'><a href ='$link'>Show course schedule</a></td>";

        $out .= "</tr>";
    }
    $out .= "</table>";
    echo $out;
    echo "wedq";
}

function studentDetail()
{
    $studentID = getParameter("student_id");
    if($studentID == -1){
        die("Student id must be provided");
    }
    $queryResult = runQuery("SELECT * FROM Student s WHERE s.sid = $studentID");
    if ($studentDetail = $queryResult->fetch_assoc()) {

        $fname = $studentDetail["fname"];
        $lname = $studentDetail["lname"];
        $birthdate = $studentDetail["birthdate"];
        $birthplace = $studentDetail["birthplace"];
    ?>


            <form action="main.php?action=save_student_detail" method="post">
        <table>
                <tr>
                    <td>First name:</td>
                    <td><input name="fname" value="<?php echo $fname ?>">
                        <input name="student_id" hidden="true" value="<?php echo $studentID ?>"></td>
                </tr>
                <tr>
                    <td>Last name:</td>
                    <td><input name="lname" value="<?php echo $lname ?>"></td>
                </tr>
                <tr>
                    <td>Birth date:</td>
                    <td><input type="date" name="birthdate" value="<?php echo $birthdate ?>"></td>
                </tr>
                <tr>
                    <td>Birth place:</td>
                    <td><input name="birthplace" value="<?php echo $birthplace ?>"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Submit"></td>
                </tr>
        </table>

            </form>
        <?php
    } else{
        echo "Student with ID cannot be found";
    }

}

function saveStudent(){

    $studentID = getParameter("student_id");
    if($studentID == -1){
        die("Student id should be provided");
    }
    $fname = getParameter("fname");
    $lname = getParameter("lname");
    $birthdate = getParameter("birthdate");
    $birthplace = getParameter("birthplace");
    runQuery("UPDATE Student SET Student.fname = '$fname', Student.lname = '$lname',
Student.birthdate = '$birthdate', Student.birthplace = '$birthplace' WHERE Student.sid = $studentID ");
    return 1;

}

function availableCourses()
{

    $departmentID = getParameter("department_id");
    $studentID = getParameter("student_id");

    if ($studentID == -1) {
        die("Student ID must be provided.");
    }

    echo $studentID . "<br>" . $departmentID;

    $studentDetail = runQuery("SELECT * FROM Student s WHERE s.sid = $studentID;");

    if ($detail = $studentDetail->fetch_assoc()) {
        $detailText = "Student No:";
        $detailText .= $detail["sid"] . " ";
        $detailText .= "Full Name:";
        $detailText .= $detail["fname"] . " " . $detail["lname"];
        ?>
        <div>
            <a></a>
        </div>
    <?php

    }

    echo $detailText;

    ?>
    <form action="main.php" method="post">
        <input name="action" hidden="true" value="available_courses">
        <input name="student_id" hidden="true" value="<?php echo $studentID ?>">
        <select name="department_id">
            <?php
            $dropdown = "";

            $departmentList = runQuery("SELECT D.dname, D.did FROM Department D;");

            while ($linea = $departmentList->fetch_assoc()) {

                $selected = "";
                $dropdown .= "<option " . ($linea["did"] == $departmentID ? "selected ='selected'" : '') . " value=" . $linea["did"] . ">" . $linea["dname"] . "</option>";

            }
            echo $dropdown;

            ?>
        </select>
        <input type="submit" value="Show courses">
    </form>
    <?php

    if ($departmentID != -1 && $studentID != -1) {


        $courses = runQuery("SELECT s.*,c.*,r.description as roomName,teacher.fname as teacherName, teacher.lname as teacherLastName, teacher.tid as teacherID
FROM Course c, schedule s, Room r, Teach teach, Teacher teacher
WHERE $departmentID = c.did AND c.did = s.cid AND r.rid = s.rid AND teach.cid = c.cid
      AND teach.tid = teacher.tid GROUP BY c.cid, r.rid;");
        $tableHTML = "<table>";

        $tableHTML .= "<tr>";
        $tableHTML .= "<th>Course Code /Class</th>";
        $tableHTML .= "<th>Course Name</th>";
        $tableHTML .= "<th>Credit</th>";
        $tableHTML .= "<th>Lecturer</th>";
        $tableHTML .= "<th>Add</th>";
        $tableHTML .= "</tr>";


        while ($courseDetail = $courses->fetch_assoc()) {
            $courseID = $courseDetail["cid"];
            $tableHTML .= "<tr>";

            $tableHTML .= "<td>";
            $tableHTML .= "<a href='main.php?action=schedule_of_course&course_id=$courseID'>";
            $tableHTML .= $courseDetail["roomName"] . "/" . $courseDetail["description"];
            $tableHTML .= "</a>";
            $tableHTML .= "</td>";

            $tableHTML .= "<td>";
            $tableHTML .= $courseDetail["title"];
            $tableHTML .= "</td>";

            $tableHTML .= "<td>";
            $tableHTML .= $courseDetail["credits"];
            $tableHTML .= "</td>";

            $teacherID = $courseDetail["teacherID"];

            $tableHTML .= "<td>";
            $tableHTML .= "<a href='main.php?action=schedule_of_teacher&teacher_id=$teacherID'>";
            $tableHTML .= $courseDetail["teacherName"] . " " . $courseDetail["teacherLastName"];
            $tableHTML .= "</a>";
            $tableHTML .= "</td>";


            $addLink = "main.php?action=take_course&student_id=$studentID&course_id=$courseID";

            $tableHTML .= "<td>";

            $tableHTML .= "<a href='$addLink' >Add</a>";

            $tableHTML .= "</td>";

            $tableHTML .= "</tr>";

        }

        $tableHTML .= "</table>";

        echo $tableHTML;
    }

}

function takeCourse()
{
    $studentID = getParameter("student_id");
    $courseID = getParameter("course_id");

    if ($studentID == -1 || $courseID == -1) {
        return -1;
    } else {

        runQuery("
    INSERT INTO Take (sid,cid)
      VALUES ($studentID, $courseID);");

        return 1;
    }
}

function coursesTaken()
{


    $studentID = getParameter("student_id");
    echo $studentID;

    if ($studentID == -1) {
        die("Student ID must be provided");
    }

    $coursesList = runQuery("
    SELECT *
    FROM Take T, Course c WHERE T.sid = $studentID AND T.cid = c.cid ;");
    ?>
    <table id="courses_list">
        <tr>
            <th>Code</th>
            <th>Name</th>
            <th>Credit</th>
            <th>Delete</th>
        </tr>
        <?php
        $tableRows = "";
        $rowNum = 0;
        while ($course = $coursesList->fetch_assoc()) {


            $courseID = $course["cid"];

            $rowName = "course_" . $rowNum;
            $row = "<tr id='$rowName'>";

            $row .= "<td>";
            $row .= "<a href = main.php?action=schedule_of_course&course_id=$courseID>";
            $row .= $course["title"];
            $row .= "</a>";
            $row .= "</td>";

            $row .= "<td>";
            $row .= "<a href = main.php?action=schedule_of_course&course_id=$courseID>";
            $row .= $course["description"];
            $row .= "</a>";
            $row .= "</td>";

            $row .= "<td>" . $course["credits"] . "</td>";
            $link = "main.php?action=remove_course_from_student&course_id=$courseID&student_id=$studentID";
            $row .= "<td><a href='$link'>Remove</a></td>";
            $row .= "</tr>";
            $tableRows .= $row;
            $rowNum++;

        }

        echo $tableRows;
        ?>


    </table>
<?php
}

function removeCourseFromStudent()
{

    $studentID = getParameter("student_id");
    $courseID = getParameter("course_id");

    if ($studentID == -1 || $courseID == -1) {
        return -1;
    }

    $sql = "
    DELETE FROM Take WHERE Take.cid = $courseID AND Take.sid = $studentID;";
    runQuery($sql);
    return 1;
}


function scheduleForStudent(){
$studentID = getParameter("student_id");

if ($studentID == -1) {
    die("Student ID must be provided");
}
$courseSchedule = runQuery("SELECT c.cid, c.description,c.title,s.dayOfWeek,s.hourOfDay, teacher.fname, teacher.lname, teacher.tid,r.description as roomName, r.rid as roomID  FROM Room r, Course c, schedule s, Take take, Teach teach, Teacher teacher
    WHERE take.sid = $studentID AND take.cid = c.cid AND c.cid = teach.cid AND teacher.tid = teach.tid and s.cid = c.cid and r.rid = s.rid
    ORDER BY length(s.hourOfDay) asc, s.hourOfDay asc
      , length (s.dayOfWeek ) asc, s.dayOfWeek asc
    ;");


?>
<table class="scheduler">
    <tr>
        <th></th>
        <th>Monday</th>
        <th>Tuesday</th>
        <th>Wednesday</th>
        <th>Thursday</th>
        <th>Friday</th>
    </tr>
    <?php
    $rows = "";
    $assocArray = array();
    while ($course = $courseSchedule->fetch_assoc()) {
        $currentHourOfDay = (int)$course["hourOfDay"];
        $currentDay = (int)$course["dayOfWeek"];

        $index = 24 * $currentDay + $currentHourOfDay;

        $assocArray[$index] = $course;

    }

    $rows .= createRowsForStudentSchedule($assocArray);


    echo $rows;
    }


    function createRowsForStudentSchedule($array)
    {

        $rows = "";

        for ($hour = 8; $hour < 17; $hour++) {
            $rows .= "<tr><td>$hour</td>";
            for ($day = 1; $day < 6; $day++) {
                $rows .= "<td>";
                if (isset($array[24 * $day + $hour])) {
                    $element = $array[24 * $day + $hour];
                    $cid = $element["cid"];

                    $teacherID = $element["tid"];
                    $classsroomID = $element["roomID"];

                    $rows .= "<a href='main.php?action=schedule_of_course&course_id=$cid'>" . $element["description"] . "</a>
                    <br><a href='main.php?action=schedule_of_teacher&teacher_id=$teacherID'>"
                        . $element["fname"] . " " . $element["lname"] . "</a><br><a href='main.php?action=schedule_of_classroom&classroom_id=$classsroomID'>" . $element["roomName"] . "</a>";
                }
                $rows .= "</td>";
            }
            $rows .= "</tr>";
        }
        return $rows;

    }


    function scheduleForLecturer(){

    $teacher_id = getParameter("teacher_id");
    if($teacher_id == -1){
        die("course id must be provided");
    }
    $teacherSchedule = runQuery("
SELECT s.dayOfWeek, s.hourOfDay, c.description, c.title, c.cid, s.rid, r.description as roomName,teacher.tid
FROM Teacher teacher, Teach teach, schedule s, Course c, Room r
WHERE teacher.tid = $teacher_id AND teach.tid = teacher.tid AND teach.cid = c.cid AND c.cid = s.cid and s.rid = r.rid
GROUP BY s.dayOfWeek, s.hourOfDay;");
    ?>


    <table class="scheduler">
        <tr>
            <th></th>
            <th>Monday</th>
            <th>Tuesday</th>
            <th>Wednesday</th>
            <th>Thursday</th>
            <th>Friday</th>
        </tr>
        <?php
        $rows = "";
        $assocArray = array();
        while($class = $teacherSchedule->fetch_assoc()){
            $currentHourOfDay = (int)$class["hourOfDay"];
            $currentDay = (int)$class["dayOfWeek"];

            $index =  24*$currentDay + $currentHourOfDay;

            $assocArray[$index] = $class;

        }

        $rows .= createRowsTeacher($assocArray);



        echo $rows;

        ?>


    </table>
    <?php
    }

    function createRowsTeacher($array){

        $rows = "";

        for($hour = 8; $hour < 17; $hour++){
            $rows .= "<tr><td>$hour</td>";
            for($day = 1; $day < 6; $day++){
                $rows .= "<td>";
                if(isset($array[24*$day+$hour])){
                    $element = $array[24*$day+$hour];
                    $courseID = $element["cid"];
                    $roomID = $element["rid"];
                    $rows .= "<a href='main.php?action=schedule_of_course&course_id=$courseID'>".
                        $element["description"]."<br>".$element["title"]."</a><br>
                        <a href='main.php?action=schedule_of_classroom&classroom_id=$roomID'>".$element["roomName"]."</a>";
                }
                $rows .= "</td>";
            }
            $rows .= "</tr>";
        }
        return $rows;

    }

    function classroomSchedule(){

    $classroomID = getParameter("classroom_id");
    if($classroomID == -1){
        die("course id must be provided");
    }
    $classroomSchedule = runQuery("SELECT s.hourOfDay, s.dayOfWeek, c.description as courseDescription, c.title, r.description, c.cid, s.rid
FROM Course c, schedule s, Room r
WHERE c.cid = s.cid AND s.rid = $classroomID AND
      s.rid = r.rid;");
    ?>


    <table class="scheduler">
        <tr>
            <th></th>
            <th>Monday</th>
            <th>Tuesday</th>
            <th>Wednesday</th>
            <th>Thursday</th>
            <th>Friday</th>
        </tr>
        <?php
        $rows = "";
        $assocArray = array();
        while($class = $classroomSchedule->fetch_assoc()){
            $currentHourOfDay = (int)$class["hourOfDay"];
            $currentDay = (int)$class["dayOfWeek"];

            $index =  24*$currentDay + $currentHourOfDay;

            $assocArray[$index] = $class;

        }

        $rows .= createRowsForClassroomSchedule($assocArray);



        echo $rows;
        ?>
        </table>
        <?php

    }

    function createRowsForClassroomSchedule($array){

        $rows = "";

        for($hour = 8; $hour <= 17; $hour++){
            $rows .= "<tr><td>$hour</td>";
            for($day = 1; $day < 6; $day++){
                $rows .= "<td>";
                if(isset($array[24*$day+$hour])){
                    $element = $array[24*$day+$hour];
                    $courseID= $element["cid"];
                    $classroomID = $element["rid"];

                    $rows .= "<a href='main.php?action=schedule_of_course&course_id=$courseID'>".
                        $element["courseDescription"]."<br>".$element["title"].
                        "</a><br><a href='main.php?action=schedule_of_classroom&classroom_id=$classroomID'>".
                        $element["description"]."</a >"."<br>"."<a>".""."</a>";
                }
                $rows .= "</td>";
            }
            $rows .= "</tr>";
        }
        return $rows;

    }

    function courseSchedule(){

        $courseID = getParameter("course_id");
        if($courseID == -1){
            die("course id must be provided");
        }
        $classroomSchedule = runQuery("
SELECT s.dayOfWeek, s.hourOfDay, c.description as courseDescription, c.title, c.cid, s.rid, r.description
FROM Teacher teacher, Teach teach, schedule s, Course c, Room r
WHERE c.cid = $courseID AND teach.tid = teacher.tid AND teach.cid = c.cid AND c.cid = s.cid and s.rid = r.rid");
        ?>


        <table class="scheduler">
            <tr>
                <th></th>
                <th>Monday</th>
                <th>Tuesday</th>
                <th>Wednesday</th>
                <th>Thursday</th>
                <th>Friday</th>
            </tr>
            <?php
            $rows = "";
            $assocArray = array();
            while($class = $classroomSchedule->fetch_assoc()){
                $currentHourOfDay = (int)$class["hourOfDay"];
                $currentDay = (int)$class["dayOfWeek"];

                $index =  24*$currentDay + $currentHourOfDay;

                $assocArray[$index] = $class;
                echo $index."<br>";

            }

            $rows .= createRowsForClassroomSchedule($assocArray);



            echo $rows;
            ?>
        </table>
    <?php

    }


    ?>


</html>