<?php
/**
 * Created by PhpStorm.
 * User: oguz
 * Date: 06/04/15
 * Time: 23:34
 */

include 'utils.php';

$sql = "
CREATE TABLE IF NOT EXISTS   Course   (
    cid   int(11) NOT NULL,
    title   varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
    description   varchar(50) COLLATE utf8mb4_turkish_ci DEFAULT NULL,
    credits   int(11) DEFAULT NULL,
    did   int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Dumping data for table   Course
--

INSERT INTO   Course   (  cid  ,   title  ,   description  ,   credits  ,   did  ) VALUES
(1, 'database', 'CENG 351', 3, 1),
(2, 'operating system', 'CENG 341', 3, 1),
(3, 'Introduction to Programming', 'CENG 101', 4, 1),
(4, 'introduction to electronic', 'EE 101', 2, 2),
(5, 'statistic', 'IE 301', 4, 4),
(6, 'circuit theory', 'EE 202', 3, 2),
(7, 'introduction to environment', 'ENV 102', 3, 3),
(8, 'operation research', 'IE 208', 3, 4),
(9, 'summer practice', 'IE 299', 2, 4),
(10, 'summer practice', 'ENV 299', 3, 3),
(11, 'summer practice', 'CENG 299', 3, 1),
(12, 'summer practice', 'EE 299', 3, 2),
(13, 'database', 'CENG 351', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table   Department
--

CREATE TABLE IF NOT EXISTS   Department   (
    did   int(11) NOT NULL,
    dname   varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
    comments   varchar(50) COLLATE utf8mb4_turkish_ci DEFAULT NULL,
    email   varchar(30) COLLATE utf8mb4_turkish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Dumping data for table   Department
--

INSERT INTO   Department   (  did  ,   dname  ,   comments  ,   email  ) VALUES
(1, 'Comp. Eng.', 'Computer Eng. Department', 'ceng@fatih.edu.tr'),
(2, 'Elec. Eng.', 'Electronic Eng. Department', 'ee@fatih.edu.tr'),
(3, 'Env. Eng.', 'Environmental Eng. Department', 'env@fatih.edu.tr'),
(4, 'Ind. Eng.', 'Industrial Eng. Department', 'ie@fatih.edu.tr');

-- --------------------------------------------------------

--
-- Table structure for table   Room
--

CREATE TABLE IF NOT EXISTS   Room   (
  rid   int(11) NOT NULL,
    description   varchar(50) COLLATE utf8mb4_turkish_ci DEFAULT NULL,
    capacity   int(11) DEFAULT NULL,
    did   int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Dumping data for table   Room
--

INSERT INTO   Room   (  rid  ,   description  ,   capacity  ,   did  ) VALUES
(1, 'LAB A', 40, 1),
(2, 'LAB B', 80, 1);

-- --------------------------------------------------------

--
-- Table structure for table   schedule
--

CREATE TABLE IF NOT EXISTS   schedule   (
    cid   int(11) NOT NULL,
    rid   int(11) NOT NULL,
    dayOfWeek   char(1) COLLATE utf8mb4_turkish_ci NOT NULL,
    hourOfDay   char(2) COLLATE utf8mb4_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Dumping data for table   schedule
--

INSERT INTO   schedule   (  cid  ,   rid  ,   dayOfWeek  ,   hourOfDay  ) VALUES
(1, 1, '1', '13'),
(1, 1, '1', '14'),
(3, 1, '1', '10'),
(3, 1, '1', '11'),
(4, 1, '3', '12'),
(6, 1, '5', '10'),
(9, 1, '4', '4'),
(13, 2, '2', '10'),
(13, 2, '2', '9');

-- --------------------------------------------------------

--
-- Table structure for table   Student
--

CREATE TABLE IF NOT EXISTS   Student   (
    sid   int(11) NOT NULL,
    fname   varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
    lname   varchar(30) COLLATE utf8mb4_turkish_ci DEFAULT NULL,
    birthdate   date DEFAULT NULL,
    birthplace   varchar(50) COLLATE utf8mb4_turkish_ci DEFAULT NULL,
    did   int(11) DEFAULT NULL,
    TCKIMLIKNO   varchar(11) COLLATE utf8mb4_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Dumping data for table   Student
--

INSERT INTO   Student   (  sid  ,   fname  ,   lname  ,   birthdate  ,   birthplace  ,   did  ,   TCKIMLIKNO  ) VALUES
(1, 'Ali', 'Turan', '0000-00-00', 'istanbul', 1, ''),
(2, 'Ahmet', 'buyuk', '0000-00-00', 'ankara', 1, ''),
(3, 'Leyla', 'Sahin', '0000-00-00', 'izmir', 1, ''),
(4, 'Can', 'Turkoglu', '0000-00-00', 'manisa', 2, ''),
(5, 'Aziz', 'Keskin', '0000-00-00', 'istanbul', 2, ''),
(6, 'Talat', 'Sanli', '0000-00-00', 'izmir', 3, ''),
(7, 'Kamuran', 'Kece', '0000-00-00', 'adana', 3, ''),
(8, 'Turgut', 'Cemal', '0000-00-00', 'bursa', 4, ''),
(9, 'Oznur', 'Gunes', '0000-00-00', 'bolu', 2, ''),
(10, 'Pelin', 'Tugay', '0000-00-00', 'izmir', 4, ''),
(11, 'Savas', 'Tan', '0000-00-00', 'izmir', 4, ''),
(12, 'Oğuz', 'Uzman', '2015-04-04', 'istanbul', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table   Take
--

CREATE TABLE IF NOT EXISTS   Take   (
    sid   int(11) NOT NULL,
    cid   int(11) NOT NULL,
    grade   float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Dumping data for table   Take
--

INSERT INTO   Take   (  sid  ,   cid  ,   grade  ) VALUES
(1, 6, 3),
(1, 9, 4),
(1, 10, 3),
(1, 13, NULL),
(2, 1, 4),
(2, 2, 4),
(2, 3, 4),
(2, 4, 4),
(2, 5, 4),
(2, 6, 4),
(2, 7, 4),
(2, 8, 4),
(2, 9, 4),
(2, 10, 3),
(2, 11, 4),
(3, 1, 4),
(3, 6, 4),
(3, 7, 4),
(3, 9, NULL),
(3, 11, 3.5),
(4, 1, 2.5),
(4, 5, 1.5),
(5, 1, 3),
(5, 5, 1.5),
(5, 11, 3.5),
(6, 2, 4),
(7, 1, 2.5),
(7, 2, 3),
(7, 5, 1.5),
(7, 8, 1.5),
(8, 2, 3.5),
(8, 7, 1.5),
(10, 2, 4),
(10, 8, 3),
(11, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table   Teach
--

CREATE TABLE IF NOT EXISTS   Teach   (
    tid   int(11) NOT NULL,
    cid   int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Dumping data for table   Teach
--

INSERT INTO   Teach   (  tid  ,   cid  ) VALUES
(1, 1),
(1, 11),
(1, 13),
(2, 3),
(3, 2),
(4, 4),
(4, 6),
(4, 12),
(5, 7),
(5, 10),
(6, 8),
(7, 5),
(7, 9);

-- --------------------------------------------------------

--
-- Table structure for table   Teacher
--

CREATE TABLE IF NOT EXISTS   Teacher   (
    tid   int(11) NOT NULL,
    fname   varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
    lname   varchar(30) COLLATE utf8mb4_turkish_ci DEFAULT NULL,
    birthdate   date DEFAULT NULL,
    birthplace   varchar(50) COLLATE utf8mb4_turkish_ci DEFAULT NULL,
    did   int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Dumping data for table   Teacher
--

INSERT INTO   Teacher   (  tid  ,   fname  ,   lname  ,   birthdate  ,   birthplace  ,   did  ) VALUES
(1, 'Selami', 'Durgun', '0000-00-00', 'amasya', 1),
(2, 'Cengiz', 'Tahir', '0000-00-00', 'istanbul', 1),
(3, 'Derya', 'Seckin', '0000-00-00', 'mersin', 1),
(4, 'Dogan', 'Gedikli', '0000-00-00', 'istanbul', 2),
(5, 'Ayten', 'Kahraman', '0000-00-00', 'istanbul', 3),
(6, 'Tahsin', 'Ugur', '0000-00-00', 'izmir', 4),
(7, 'Selcuk', 'Ozan', '0000-00-00', 'amasya', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table   Course
--
ALTER TABLE   Course
 ADD PRIMARY KEY (  cid  );

--
-- Indexes for table   Department
--
ALTER TABLE   Department
 ADD PRIMARY KEY (  did  );

--
-- Indexes for table   Room
--
ALTER TABLE   Room
 ADD PRIMARY KEY (  rid  );

--
-- Indexes for table   schedule
--
ALTER TABLE   schedule
 ADD PRIMARY KEY (  cid  ,  rid  ,  dayOfWeek  ,  hourOfDay  );

--
-- Indexes for table   Student
--
ALTER TABLE   Student
 ADD PRIMARY KEY (  sid  );

--
-- Indexes for table   Take
--
ALTER TABLE   Take
 ADD PRIMARY KEY (  sid  ,  cid  );

--
-- Indexes for table   Teach
--
ALTER TABLE   Teach
 ADD PRIMARY KEY (  tid  ,  cid  );

--
-- Indexes for table   Teacher
--
ALTER TABLE   Teacher
 ADD PRIMARY KEY (  tid  );

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table   Room
--
ALTER TABLE   Room
MODIFY   rid   int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

";

runQuery($sql);