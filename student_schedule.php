<!DOCTYPE html>
<html>


<head>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<?php
/**
 * Created by PhpStorm.
 * User: oguz
 * Date: 05/04/15
 * Time: 00:50
 */

include "utils.php";

$studentID = getParameter("student_id");

if($studentID == -1){
    die("Student ID must be provided");
}
$courseSchedule = runQuery("SELECT c.cid, c.description,c.title,s.dayOfWeek,s.hourOfDay, teacher.fname, teacher.lname,r.description as roomName  FROM Room r, Course c, schedule s, Take take, Teach teach, Teacher teacher
    WHERE take.sid = $studentID AND take.cid = c.cid AND c.cid = teach.cid AND teacher.tid = teach.tid and s.cid = c.cid and r.rid = s.rid
    ORDER BY length(s.hourOfDay) asc, s.hourOfDay asc
      , length (s.dayOfWeek ) asc, s.dayOfWeek asc
    ;");


?>
<table class="scheduler">
    <tr>
    <th></th>
    <th>Monday</th>
    <th>Tuesday</th>
    <th>Wednesday</th>
    <th>Thursday</th>
    <th>Friday</th>
    </tr>
    <?php
    $rows = "";
    $assocArray = array();
    while($course = $courseSchedule->fetch_assoc()){
        $currentHourOfDay = (int)$course["hourOfDay"];
        $currentDay = (int)$course["dayOfWeek"];

        $index =  24*$currentDay + $currentHourOfDay;

        $assocArray[$index] = $course;

    }

    $rows .= createRowsForStudentSchedule($assocArray);



    echo $rows;

    ?>


</table>



<?php

function createRowsForStudentSchedule($array){

    $rows = "";

    for($hour = 8; $hour < 17; $hour++){
        $rows .= "<tr><td>$hour</td>";
        for($day = 1; $day < 6; $day++){
            $rows .= "<td>";
            if(isset($array[24*$day+$hour])){
                $element = $array[24*$day+$hour];

                $rows .= "<a>".$element["description"]."<br>"
                    .$element["fname"]." ".$element["lname"]."<br>".$element["roomName"]."</a>";
            }
            $rows .= "</td>";
        }
        $rows .= "</tr>";
    }
    return $rows;

}
printBanner();
?>
<a href="student_list.php">Return to student list</a>